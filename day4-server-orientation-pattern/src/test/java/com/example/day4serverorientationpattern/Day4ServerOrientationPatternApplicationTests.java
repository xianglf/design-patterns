package com.example.day4serverorientationpattern;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class Day4ServerOrientationPatternApplicationTests {

	@Resource
	private ParserFactory parserFactory;
	@Test
	void contextLoads() {
		ContentType contentType = ContentType.getByCode("csv");
		parserFactory.getParser(contentType).Parse(null);
	}

}
