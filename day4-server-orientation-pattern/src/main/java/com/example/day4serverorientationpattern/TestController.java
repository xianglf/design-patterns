package com.example.day4serverorientationpattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:14
 */
@RestController
public class TestController {
    @Autowired
    private ParserFactory parserFactory;

    public void test(){
        parserFactory.getParser(ContentType.JSON).Parse(null);
    }
}
