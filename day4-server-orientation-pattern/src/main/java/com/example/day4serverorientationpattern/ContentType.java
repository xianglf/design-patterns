package com.example.day4serverorientationpattern;

import java.util.Arrays;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:11
 */
public enum ContentType {
    JSON("json","JSON"),CSV("csv","CSV");

    private String code;
    private String value;

    ContentType(String code,String value){
        this.code = code;
        this.value = value;
    }
    public String getCode() {
        return this.code;
    }
    public static ContentType getByCode(String code){
        return Arrays.stream(ContentType.values()).filter(e -> code.equals(e.getCode())).findFirst().get();
    }
}
