package com.example.day4serverorientationpattern;

import org.springframework.stereotype.Component;

import java.io.Reader;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:13
 */
@Component("JSON")
public class JSONParser implements Parser{
    @Override
    public void Parse(Reader r) {
        System.out.println("JSON解析");
    }
}
