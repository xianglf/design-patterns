package com.example.day4serverorientationpattern;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:15
 */
public interface ParserFactory {
    Parser getParser(ContentType contentType);
}
