package com.example.day4serverorientationpattern;

import java.io.Reader;
import java.util.List;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:10
 */
public interface Parser {
    void Parse(Reader r);
}
