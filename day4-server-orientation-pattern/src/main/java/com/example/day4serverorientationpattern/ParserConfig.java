package com.example.day4serverorientationpattern;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:15
 */
@Configuration
public class ParserConfig {

    @Bean("parserFactory")
    public FactoryBean<Object> serverOrientationBean(){
        ServiceLocatorFactoryBean factoryBean = new ServiceLocatorFactoryBean();
        factoryBean.setServiceLocatorInterface(ParserFactory.class);
        return factoryBean;
    }
}
