package com.example.day4serverorientationpattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day4ServerOrientationPatternApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day4ServerOrientationPatternApplication.class, args);
	}

}
