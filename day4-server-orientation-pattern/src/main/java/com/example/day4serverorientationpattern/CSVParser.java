package com.example.day4serverorientationpattern;

import org.springframework.stereotype.Component;

import java.io.Reader;
import java.util.List;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 10:12
 */
@Component("CSV")
public class CSVParser implements Parser{
    @Override
    public void Parse(Reader r) {
        System.out.println("CSV解析");
    }
}
