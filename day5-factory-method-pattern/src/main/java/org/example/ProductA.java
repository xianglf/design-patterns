package org.example;

/**
 * @author xianglongfei
 * @description 产品A
 * @createDate 2023/3/3 9:09
 */
public class ProductA extends Product{

    /**
     * @description:
     * @Author: xianglongfei
     * @Date: 2023/03/03 09:10:62
     */
    @Override
    public void show(String mode) {
        System.out.println(mode+"方式产出产品A");
    }
}
