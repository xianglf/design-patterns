package org.example;

public class FactoryMethodMain {
    public static void main(String[] args) {
        IFactory factoryA = new ProductAFactory();
        factoryA.createProduct().show("Factory Method");

        IFactory factoryB = new ProductBFactory();
        factoryB.createProduct().show("Factory Method");
    }
}