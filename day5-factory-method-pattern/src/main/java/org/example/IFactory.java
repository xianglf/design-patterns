package org.example;

/**
 * @author xianglongfei
 * @description 工厂类
 * @createDate 2023/3/3 9:13
 */
public interface IFactory {
    Product createProduct();
}
