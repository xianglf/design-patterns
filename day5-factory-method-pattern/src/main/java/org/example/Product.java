package org.example;

/**
 * @author xianglongfei
 * @description 抽象产品类
 * @createDate 2023/3/3 9:08
 */
abstract class Product {
    abstract void show(String mode);
}
