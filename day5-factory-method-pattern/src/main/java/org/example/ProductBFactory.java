package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/7 9:41
 */
public class ProductBFactory implements IFactory{
    @Override
    public Product createProduct() {
        return new ProductB();
    }
}
