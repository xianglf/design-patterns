package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/7 9:40
 */
public class ProductAFactory implements IFactory{
    @Override
    public Product createProduct() {
        return new ProductA();
    }
}
