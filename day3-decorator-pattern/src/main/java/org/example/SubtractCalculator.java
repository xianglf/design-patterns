package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/3 11:20
 */
public class SubtractCalculator implements Calculator{
    private final Calculator calculator;

    public SubtractCalculator(Calculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public void calculate(Integer integerA, Integer integerB) {
        calculator.calculate(integerA,integerB);
        int i = integerA - integerB;
        System.out.println("减法：" + i);
    }
}
