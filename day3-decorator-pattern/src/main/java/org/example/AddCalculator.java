package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/3 11:16
 */
public class AddCalculator implements Calculator {

    @Override
    public void calculate(Integer integerA, Integer integerB) {
        System.out.println("加法："+ (integerA + integerB));
    }
}
