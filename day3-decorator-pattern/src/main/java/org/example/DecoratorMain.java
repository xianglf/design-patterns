package org.example;

public class DecoratorMain {
    public static void main(String[] args) {
        Calculator calculator = new AddCalculator();
        calculator.calculate(3,2);

        new SubtractCalculator(calculator).calculate(3,2);

    }
}