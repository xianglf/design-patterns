package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/3 11:15
 */
public interface Calculator {
    void calculate(Integer integerA, Integer integerB);
}
