package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/6 9:51
 */
public class RealSubject implements Subject{
    @Override
    public void request() {
        System.out.println("真实调用");
    }
}
