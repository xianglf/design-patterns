package org.example;

/**
 * @author xianglongfei
 * @description 代理类
 * @createDate 2023/3/6 9:52
 */
public class ProxySubject implements Subject{

    private RealSubject realSubject;

    @Override
    public void request() {
        if(realSubject == null){
            realSubject = new RealSubject();
        }
        preRequest();
        realSubject.request();
        postRequest();
    }

    public void preRequest() {
        System.out.println("代理preRequest调用！");
    }

    public void postRequest() {
        System.out.println("代理postRequest调用！");
    }
}
