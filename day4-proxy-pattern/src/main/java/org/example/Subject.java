package org.example;

/**
 * @author xianglongfei
 * @description 抽象主题角色
 * @createDate 2023/3/6 9:51
 */
public interface Subject {
    void request();
}
