package org.example;

public class Main {
    public static void main(String[] args) {
        int a = 1;
        Object A = a;
        System.out.println(A instanceof Integer);
        Class<?> cls = A.getClass();
        System.out.println("Class name: " + cls.getName());
    }
}