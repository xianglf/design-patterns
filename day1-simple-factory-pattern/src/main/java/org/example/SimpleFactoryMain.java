package org.example;

public class SimpleFactoryMain {
    public static void main(String[] args) {
        Product product = Factory.createProduct("A");
        product.show("不使用依赖注入");

        DIFactory diFactory = new DIFactory("B");
        diFactory.createProduct().show("使用依赖注入");

    }


}