package org.example;

/**
 * @author xianglongfei
 * @description 使用依赖注入的工厂类
 * @createDate 2023/3/3 9:38
 */
public class DIFactory {
    private final String productType;

    public DIFactory(String productType){
        this.productType = productType;
    }

    public Product createProduct(){
        //工厂通过传入的产品类型进决定生产哪种产品
        switch (this.productType) {
            case "A":
                return new ProductA();
            case "B":
                return new ProductB();
            default:
                throw new IllegalArgumentException("没有该类型的产品！");
        }
    }
}
