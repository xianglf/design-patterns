package org.example;

/**
 * @author xianglongfei
 * @description 工厂类
 * @createDate 2023/3/3 9:13
 */
public class Factory {

    /**
     * @param productType 产品类型
     * @return {@code Product }
     * @description:
     * @Author: xianglongfei
     * @Date: 2023/03/03 09:17:62
     */
    public static Product createProduct(String productType){
        //工厂通过传入的产品类型进决定生产哪种产品
        switch (productType) {
            case "A":
                return new ProductA();
            case "B":
                return new ProductB();
            default:
                throw new IllegalArgumentException("没有该类型的产品！");
        }
    }
}
