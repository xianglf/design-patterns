package org.example;

/**
 * @author xianglongfei
 * @description 抽象产品类
 * @createDate 2023/3/3 9:08
 */
public interface ProductStrategy {
    void show(String mode);
}
