package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/3 9:57
 */
public class Context {
    private final ProductStrategy productStrategy;

    public Context(ProductStrategy productStrategy) {
        this.productStrategy = productStrategy;
    }
    public void execute(String mode){
        productStrategy.show(mode);
    }
}
