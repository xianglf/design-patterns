package org.example;

/**
 * @author xianglongfei
 * @description
 * @createDate 2023/3/3 10:00
 */
public class StrategyFactory {

    private final String productType;

    public StrategyFactory(String productType) {
        this.productType = productType;
    }

    public ProductStrategy createProductStrategy(){
        switch (this.productType){
            case "A":
                return new ProductStrategyA();
            case "B":
                return new ProductStrategyB();
            default:
                throw new IllegalArgumentException("没有该类型的产品！");
        }
    }
}
