package org.example;

public class StrategyMain {
    public static void main(String[] args) {
        ProductStrategyA productStrategyA = new ProductStrategyA();
        new Context(productStrategyA).execute("策略模式");


        new Context( new StrategyFactory("B").createProductStrategy()).execute("结合工厂模式");
    }
}