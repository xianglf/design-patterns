package org.example;

/**
 * @author xianglongfei
 * @description 产品B
 * @createDate 2023/3/3 9:12
 */
public class ProductStrategyB implements ProductStrategy {
    /**
     * @description:
     * @Author: xianglongfei
     * @Date: 2023/03/03 09:12:62
     */
    @Override
    public void show(String mode) {
        System.out.println(mode+"方式产出产品B");
    }
}
